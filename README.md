# coakt-poc

coakt-poc is a proof of concept application written in Haskell.


## Build

To build you will need to install the [Haskell Tool Stack](https://docs.haskellstack.org/en/stable/README/).
You can alternatively install the [Haskel Platform](https://www.haskell.org/platform/) which includes the some additional tools.

Once stack has completed installation, run the following commands to change into the coakt-poc directory, and build the code.

```sh
git clone https://gitlab.com/coakt/coakt-poc.git
cd coakt-poc
stack build
```

## Execute
In one command prompt start http server on port 8000, p2p server on port 8001, and set p2p seed to 8001 
```sh
stack exec coakt-poc-exe 8000 8001 127.0.0.1:8001
```

In another command prompt mimic another machine by using a different port for http server and p2p server. 

```sh
stack exec coakt-poc-exe 8002 8003 127.0.0.1:8001
```


### Interact with p2p server

Type command 'all' in either command prompt that is executing app to list all peers


### Interact with http server

To create a new user, fire up a new command prompt and:

```sh
curl -X POST -d '{ "name":"Jill" }' http://localhost:8000/users
```


