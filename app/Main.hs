module Main where

import System.Environment (getArgs)
import Server (runServer)
import P2P (runPeer)

main :: IO ()
main = do
  args <- getArgs >>= \a -> case a of
                              [h,p,s] -> return [h,p,s] 
                              _ -> fail "Usage:$ coakt-poc-exe httpPort p2pPort p2pSeed"
  runPeer (args !! 1) (args !! 2)
  runServer $ head args



