{-# LANGUAGE OverloadedStrings #-}

module P2P (runPeer) where

import Server (User (..))

import Data.Text (pack, unpack)

import Network.Transport.TCP (createTransport, defaultTCPParameters)
import Control.Distributed.Process
import Control.Distributed.Process.Node
import qualified Control.Distributed.Backend.P2P  as P2P

import Control.Concurrent (threadDelay)
import Control.Monad (forever)

import qualified Data.ByteString.Lazy as B
import Data.Aeson 
import Data.Maybe


getJSON :: IO B.ByteString
getJSON = B.readFile ".users.json"

p2pServiceName :: String
p2pServiceName = "updateservice"

runPeer :: String -> String -> IO()
runPeer port seed = do
  let host = "127.0.0.1"
  let ext = const (host, port)
  (localNode, pid) <- P2P.bootstrapNonBlocking host port ext initRemoteTable [P2P.makeNodeId seed] peerProcess
  return ()

peerProcess :: Process ()
peerProcess = do
  forever $ do
    cmd <- liftIO getLine
    case words cmd of
      ["all"] -> listPeers

listPeers :: Process ()
listPeers = P2P.getPeers >>= (liftIO . print)

{-  Right t <- createTransport "127.0.0.1" port defaultTCPParameters
  node <- newLocalNode t initRemoteTable
  d <- (eitherDecode <$> getJSON) :: IO (Either String User)
  case d of
    Left err -> putStrLn err
    Right user -> do
      runProcess node $ demoSendReceive user
-}

myUserNameMessage :: String -> String
myUserNameMessage userName = "My userName is " ++ userName

myUUIDMessage :: String -> String
myUUIDMessage uuid = "My uuid is " ++ uuid

