{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Server (runServer, User (..)) where

import GHC.Generics
import Web.Scotty
import Data.UUID (toText)
import Data.UUID.Types
import Data.UUID.V4 (nextRandom)
import Data.Text (Text)
import Data.Text.Lazy (fromStrict)
import qualified Data.Text.Lazy.IO as I (writeFile)
import Control.Monad.IO.Class
import Data.Maybe

import Data.Aeson.Text (encodeToLazyText)
import Data.Aeson 

data User = User { name :: Text, uuid :: Maybe Text } deriving (Show, Generic, ToJSON)

instance FromJSON User
  where parseJSON = genericParseJSON defaultOptions { omitNothingFields  = True }

genText :: IO Text
genText = toText <$> nextRandom

runServer :: String -> IO ()
runServer port = scotty (read port :: Int) $ do
         post "/users" $ do
          j <- jsonData :: ActionM User
          uuidv4 <- liftIO genText
          let n = name j
          let user = User { name = n, uuid = Just uuidv4 }
          let userText = encodeToLazyText user
          liftIO $ I.writeFile ".users.json" userText
          text userText

-- example
-- curl -X POST -d '{ "name":"Jill"}' http://localhost:3000/users
      
